//
//  PokemonCell.swift
//  PokemonAPP
//
//  Created by Julio Nicolas on 4/24/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import UIKit
import AlamofireImage
import Canvas
import CoreData

class PokemonCell: UICollectionViewCell {
    
    @IBOutlet var backgroundCardView:CSAnimationView?
    @IBOutlet var imagenPokemon:UIImageView?
    @IBOutlet var nombrePokemon:UILabel?;
    
    
    var obj_pokemon:Pokemon!{
        didSet{
            self.updateUI();
        }
    };
    
    var obj_pokemon_bd: NSManagedObject!{
        didSet{
            self.updateUIBD();
        }
    };

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }
    
    
    func updateUI(){
        backgroundCardView?.startCanvasAnimation();
        
        nombrePokemon?.text = obj_pokemon.name;
        imagenPokemon?.af_setImage(withURL: obj_pokemon.front_default!);
        
        backgroundCardView?.backgroundColor = UIColor.white
        contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
        
        backgroundCardView?.layer.cornerRadius = 3.0
        backgroundCardView?.layer.masksToBounds = false
        
        backgroundCardView?.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        
        backgroundCardView?.layer.shadowOffset = CGSize(width: 0, height: 0)
        backgroundCardView?.layer.shadowOpacity = 0.8

    
    }
    
    func updateUIBD(){
        backgroundCardView?.startCanvasAnimation();
        
        nombrePokemon?.text = obj_pokemon_bd.value(forKey: "name") as? String
        imagenPokemon?.af_setImage(withURL: URL(string:(obj_pokemon_bd.value(forKey: "front_default") as? String)!)!);
        
        backgroundCardView?.backgroundColor = UIColor.white
        contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
        
        backgroundCardView?.layer.cornerRadius = 3.0
        backgroundCardView?.layer.masksToBounds = false
        
        backgroundCardView?.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        
        backgroundCardView?.layer.shadowOffset = CGSize(width: 0, height: 0)
        backgroundCardView?.layer.shadowOpacity = 0.8
        
        
    }

}
