//
//  Pokemon.swift
//  PokemonAPP
//
//  Created by Julio Nicolas on 4/23/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import CoreData
import Foundation

class Pokemon : NSObject{

    var id_pokemon:Int?;
    var name:String?;
    var front_default:URL?;
    
    typealias objDictionary = [String:AnyObject];
    
    
    
    override init() {
        // perform some initialization here
    }
    
    
    init(pDictionary:objDictionary) {
        
        self.id_pokemon = (pDictionary["id_pokemon"] as? Int)!;
        self.name = pDictionary["name"] as? String;
        self.front_default = URL(string:pDictionary["front_default"] as! String);
        
        super.init()
        
    }
    
    
    static func parseData (_ jsonDic:[[String:AnyObject]])->[Pokemon] {
        var array_obj = [Pokemon]();
        //self.truncateTable();
        do{
            var indexJ = 1;
            for obj in jsonDic {
                
                var mObjDictionary = obj;
                mObjDictionary ["id_pokemon"] = indexJ as AnyObject?;
                mObjDictionary ["front_default"] = self.agregarImagenP(indexRow: indexJ) as AnyObject?;//agregar pokemon imagen
                
                let pokemon = Pokemon(pDictionary:mObjDictionary)
                
                self.insertarDB(pDictionary: mObjDictionary);
                
                indexJ += 1;
                
                array_obj.append(pokemon);
            }
            
            
        }catch let error as NSError{
            print(error);
        }
        
        
        return array_obj;
        
    }
    
    static func agregarImagenP (indexRow:Int) -> String {
        
        return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(indexRow).png"
        
    }
    
    static func insertarDB(pDictionary:objDictionary){
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let manageContext = appDelegate?.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "PokemonDB", in: manageContext!)
        
        let newPokemon = NSManagedObject(entity: entity!, insertInto: manageContext)
        
        newPokemon.setValue((pDictionary["id_pokemon"] as? Int)!, forKey: "id_pokemon")
        newPokemon.setValue(pDictionary["name"] as? String, forKey: "name")
        newPokemon.setValue(pDictionary["front_default"] as! String, forKey: "front_default")
        
        do {
            try manageContext?.save()
            
        } catch let error as NSError {
            print("No se pudo guardar: \(error), \(error.userInfo)")
        }
        
    }
    
    static func truncateTable(){
        
        // create the delete request for the specified entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PokemonDB")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        // get reference to the persistent container
        let persistentContainer = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        // perform the delete
        do {
            try persistentContainer.viewContext.execute(deleteRequest)
        } catch let error as NSError {
            print(error)
        }
        
    }
    

}

