//
//  Constants.swift
//  PokemonAPP
//
//  Created by Julio Nicolas on 4/24/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class Constants {
    static let baseColor: UIColor = UIColor(colorLiteralRed: 26/255.0, green: 121/255.0, blue: 68/255.0, alpha: 1.0)
    static let textColorNavigation: UIColor = UIColor.white
    static let primaryText: UIColor = UIColor(colorLiteralRed: 96/255.0, green: 96/255.0, blue: 89/255.0, alpha: 1.0)
    static let secondaryText: UIColor = UIColor(colorLiteralRed: 144/255.0, green: 144/255.0, blue: 144/255.0, alpha: 1.0)
}
