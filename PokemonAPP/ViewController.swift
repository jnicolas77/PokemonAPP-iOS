//
//  ViewController.swift
//  PokemonAPP
//
//  Created by Julio Nicolas on 4/23/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CoreData
import SystemConfiguration
import Foundation

class ViewController: UIViewController, NVActivityIndicatorViewable, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ManagerDelegate{
    
    
    var urlWS = "https://pokeapi.co/api/v2/";
    var urlPokemonList = "pokemon/?limit=150";
    
    var pokemon: [NSManagedObject] = []
    
    @IBOutlet var collectionViewList : UICollectionView?;
    
    
    
    var arrayPokemon:[Pokemon]!;
    
    let size_loading = CGSize(width: 30, height:30);
    let type_loading = 23;
    fileprivate let itemsPerRow: CGFloat = 2
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let nib = UINib(nibName: "PokemonCell", bundle: nil)
        self.collectionViewList?.register(nib, forCellWithReuseIdentifier: "PokemonCell")
        
        self.collectionViewList?.dataSource = self
        self.collectionViewList?.delegate = self
        
        
        if (self.isInternetAvailable()){
            print(" hay internet");
            getPokemonList();
        }else {
            print("no hay internet");
            loadBD()
        }
        
        
        //getPokemon();
        
    }
    
    
    func endPointResponse(tag: String, json: JSON) {
        if tag == "pokemon" {
        
            print("POKEMON");
            
            print(json);
            
            getPokemonByType(pokemonType: "5");
            
        }else if (tag == "type"){
            
            print("TYPE");
            
            print(json);
            
        }
    }

    func getPokemon () -> Void {
        let manager: Manager = Manager(tag:"pokemon", version:"v1");
        manager.delegate = self;
        manager.getPokemon(endpoint: "pokemon", idPokemon: "1");
        
    }
    
    func getPokemonByType (pokemonType: String) -> Void {
        let manager: Manager = Manager(tag:"type", version:"v1");
        manager.delegate = self;
        manager.getPokemonByType(endpoint: "type", pokemonType: pokemonType);
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (arrayPokemon == nil) ? 0 : arrayPokemon!.count;
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 202)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokemonCell", for: indexPath) as! PokemonCell
        
        if (self.isInternetAvailable()){
            
            let pokemon = arrayPokemon! [indexPath.row];
            cell.obj_pokemon = pokemon;
            
        }else {
            
            let pokemonbd = pokemon[indexPath.row];
            cell.obj_pokemon_bd = pokemonbd;
            
        }
        
        
        
        return cell
    }
    
    fileprivate func getPokemonList(){
        
        self.arrayPokemon = [Pokemon]();
        
        startAnimating(size_loading, message: "", type: NVActivityIndicatorType(rawValue: type_loading))
        
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        print("url \(urlWS+urlPokemonList)");
        
        Alamofire.request(urlWS+urlPokemonList, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                self.stopAnimating();
                if let data = response.result.value{
                    var arrayResultado = [[String:AnyObject]]()
                    let swiftyJsonResonse = JSON(data);
                    
                    if let resData = swiftyJsonResonse["results"].arrayObject {
                        arrayResultado = resData as! [[String:AnyObject]]
                        self.arrayPokemon = Pokemon.parseData(arrayResultado);
                    }
                
                    print(self.arrayPokemon.count);
                    
                    self.collectionViewList?.reloadData();
                    
                }else{
                    
                    self.collectionViewList?.reloadData();
                    
                }
                break
                
            case .failure(_):
                self.stopAnimating();
            
                self.collectionViewList?.reloadData();
                
                
                break
                
            }
        }
        
    
    }
    
    
    func loadBD() -> Void {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let manageContext = appDelegate?.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "PokemonDB")
        
        do {
           self.pokemon = try manageContext?.fetch(fetchRequest) as [NSManagedObject]!
            
           
            self.collectionViewList?.reloadData();
        } catch let error as NSError {
            print("No se obtener la información: \(error), \(error.userInfo)")
        }
    }
    
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    

}

